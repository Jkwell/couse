// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()
const MAX_LIMIT = 100;
const axios = require('axios')
const cheerio = require('cheerio');
const { data } = require('cheerio/lib/api/attributes');

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const _openid = wxContext.OPENID;
  switch (event.fn) {
    // 获取 用户信息
    case 'get_user': {
      return get_user(event, _openid)
    }
    // 新增 用户
    case 'add_user': {
      return add_user(event, _openid)
    }
    // 获取 个人积分 排名
    case 'get_my_grade': {
      return get_my_grade(event, _openid)
    }
    // 获取 积分排行榜
    case 'get_user_grade': {
      return get_user_grade(event, _openid)
    }
    // 获取 个人所有积分
    case 'get_gradeall': {
      return get_gradeall(event, _openid)
    }
    // 获取 个人当月积分
    case 'get_grade': {
      return get_grade(event, _openid)
    }
    // 新增 个人当月积分
    case 'add_grade': {
      return add_grade(event, _openid)
    }
    // 修改 个人当月积分
    case 'update_grade': {
      return update_grade(event, _openid)
    }
    // 获取 句子 数据
    case 'get_sentence': {
      return get_sentence(event, _openid)
    }
    // 获取 句子练习 题
    case 'get_sentence_q': {
      return get_sentence_q(event, _openid)
    }
    // 获取 组句 题
    case 'get_sg_q': {
      return get_sg_q(event, _openid)
    }
    // 获取 用户id
    case 'get_openid': {
      return _openid
    }
    // case 'get_sentence': {
    //   return get_sentence(event, _openid)
    // }

    case 'update_word': {
      return update_word(event)
    }
    case 'get_category': {
      return get_category();
    }
    case 'add_user_word': {
      return add_user_word(event, _openid);
    }
    case 'remove_user_word': {
      return remove_user_word(event, _openid);
    }
    case 'get_user_word': {
      return get_user_word(event, _openid);
    }
    case 'get_not_user_word': {
      return get_not_user_word(event, _openid);
    }
    case 'some': {
      return some(event, _openid)
    }

    case 'get_q': {
      return get_q(event, _openid)
    }

    case 'get_q_pinxie': {
      return get_q_pinxie(event, _openid)
    }
    default:
      break;
  }
}

function get_options() {
  let list = []
  this.setData({
    _id: _id
  })
  const db = wx.cloud.database()
  db.collection('word').doc(this.data._id).get()
    .then(res => {
      this.setData({
        right: res.data
      })
      list.push(res.data)
      wx.cloud.callFunction({
        name: 'api',
        data: {
          fn: 'some'
        }
      }).then(res => {
        wx.hideLoading()
        list = list.concat(res.result.list)
        list.sort((a, b) => Math.random() - 0.5)
        this.setData({
          list
        })
      })
    })
}

async function get_q_pinxie(event, _openid) {

  let ids = event.ids;
  let _ = db.command;
  let read_list = await db.collection('word').where({
    _id: _.in(ids)
  }).get()
  let list = read_list.data;
  let total = list.length;
  return {
    total: total,
    list: list,
  }
}

async function some() {
  return await db.collection('word')
    .aggregate()
    .sample({
      size: 3
    })
    .end()
}

async function update_word(event) {
  return await db.collection('word')
    .doc(event._id)
    .update({
      data: event.data
    })
}
async function get_category(event, _openid) {
  return await db.collection('category')
    .field({
      id: true,
      name: true,
      // cover: true,
    })
    .limit(100).get()
}

async function add_user_word(event, _openid) {
  let data = event.data;
  data._openid = _openid;
  data.created_at = new Date().valueOf();
  return await db.collection('user_word').add({
    data,
  })
}

async function remove_user_word(event, _openid) {
  let _id = event._id;
  return await db.collection('user_word').doc(_id).remove()
}

async function get_user_word(event, _openid) {


  let limit = event.limit || 20;
  let page = event.page || 1;
  let where = event.where || {};
  where._openid = _openid;
  let read_total = await db.collection('user_word')
    .where(where)
    .count()
  let total = read_total.total;
  let read_list = await db.collection('user_word')
    .aggregate()
    .match(where)
    .lookup({
      from: 'word',
      localField: 'word_id',
      foreignField: '_id',
      as: 'word',
    })
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({
      created_at: 1
    })
    .end()

  let list = read_list.list;
  return {
    total: total,
    list: list,
  }
}

async function get_not_user_word(event, _openid) {

  // let arr = [];
  // for (let i = 0; i < event.display_already.length; i++) {
  //   for (let j = 0; j < event.display_already[i].length; j++) {
  //     arr.push(event.display_already[i][j]._id)
  //   }
  // }


  let limit = event.limit || 20;
  let page = event.page || 1;
  let where = event.where || {};

  where.bookId = where.bookId

  // let _ = db.command;
  // where._id = _.nin(arr)

  let read_total = await db.collection('word')
    .where(where)
    .count()
  let total = read_total.total;
  let read_list = await db.collection('word')
    .where(where)
    .skip((page - 1) * limit)
    .limit(limit)
    .orderBy('created_at', 'desc')
    .get()
  let list = read_list.data;
  return {
    total: total,
    list: list,
  }
}

async function get_all(name, where) {
  // 先取出集合记录总数
  const countResult = await db.collection(name).where(where).count()
  const total = countResult.total
  // 计算需分几次取
  const batchTimes = Math.ceil(total / 100)

  if (batchTimes == 0) {
    return {
      data: []
    }
  }
  // 承载所有读操作的 promise 的数组
  const tasks = []
  for (let i = 0; i < batchTimes; i++) {
    const promise = db.collection(name).where(where).field({
      _id: true,
      word_id: true,
    }).skip(i * MAX_LIMIT).limit(MAX_LIMIT).get()
    tasks.push(promise)
  }
  // 等待所有
  return (await Promise.all(tasks)).reduce((acc, cur) => {
    return {
      data: acc.data.concat(cur.data),
      errMsg: acc.errMsg,
    }
  })
}

async function get_my_grade(event, _openid) {
  var $ = db.command.aggregate
  let _ = db.command;
  let list;
  let myrank = 0;
  let mygradeVal = await db.collection('grade').where({ openid: _openid, month: event.data.month, year: event.data.year }).get()
  let mygrade = mygradeVal.data[0].grade;
  let myrankVal = await db.collection('grade').where({ month: event.data.month, year: event.data.year, grade: _.gte(mygrade) }).count()
  myrank = myrankVal.total;
  await db.collection('grade')
    .aggregate()
    .lookup({
      from: 'user',
      localField: 'openid',
      foreignField: 'openid',
      as: 'user_grade'
    })
    .replaceRoot({
      newRoot: $.mergeObjects([$.arrayElemAt(['$user_grade', 0]), '$$ROOT'])
    })
    .project({
      user_grade: 0
    })
    .match({
      openid: _openid,
      month: event.data.month,
      year: event.data.year
    })
    .end()
    .then(res => {
      list = res.list;
    })

  return {
    list: list,
    myrank: myrank
  }
}

async function get_user_grade(event, _openid) {
  var $ = db.command.aggregate
  let limit = event.limit || 20;
  let page = event.page || 1;
  let pageNum = (page - 1) * limit;
  let totalVal = await db.collection('grade').where({ year: event.data.year, month: event.data.month }).count()
  let list = [];
  let total = totalVal.total;

  await db.collection('grade')
    .aggregate()
    .lookup({
      from: 'user',
      localField: 'openid',
      foreignField: 'openid',
      as: 'user_grade'
    })
    .replaceRoot({
      newRoot: $.mergeObjects([$.arrayElemAt(['$user_grade', 0]), '$$ROOT'])
    })
    .project({
      user_grade: 0
    })
    .match({
      year: event.data.year,
      month: event.data.month
    })
    .sort({
      grade: -1
    })
    .skip(pageNum)
    .limit(limit)
    .end()
    .then(res => {
      list = res.list;
    })

  return {
    total: total,
    list: list
  }
}

async function get_user(event, _openid) {
  return await db.collection('user').where({ openid: _openid }).get()
}

async function add_user(event, _openid) {
  return await db.collection('user').add({
    data: {
      openid: _openid,
      created_date: new Date(),
      avatarUrl: event.data.avatarUrl,
      nickName: event.data.nickName
    }
  }).then(res => {
    console.log(res);
  })
}

async function get_gradeall(event, _openid) {
  return await db.collection('grade').where({ openid: _openid }).orderBy('create_date', 'desc').get()
}

async function get_grade(event, _openid) {
  return await db.collection('grade').where({ openid: _openid, month: event.data.month, year: event.data.year }).get()
}

async function add_grade(event, _openid) {
  return await db.collection('grade').add({ data: event.data })
}

async function update_grade(event, _openid) {
  return await db.collection('grade').doc(event.data.id).update({
    data: {
      grade: event.data.grade
    }
  })
}

async function get_sentence(event, _openid) {
  var $ = db.command.aggregate
  let limit = event.limit || 20;
  let page = event.page || 1;
  let pageNum = (page - 1) * limit;
  let totalVal = await db.collection('sentence').where({ pid: event.data.pid }).count()
  let list = [];
  let total = totalVal.total;
  let senV = await db.collection('sentence')
    .where({ pid: event.data.pid })
    .skip(pageNum)
    .limit(limit)
    .get()
  list = senV.data;
  return {
    total: total,
    list: list
  }
}

async function get_q(event, _openid) {
  // 接收发送过来的单词
  // 生成题目
  let ids = event.ids;
  let _ = db.command;
  let read_list = await db.collection('word').where({
    _id: _.in(ids)
  }).get()
  let list = read_list.data;
  let total = list.length;
  // 取total*3个单词 用来混入正确答案中
  let ops = ['A', 'B', 'C', 'D']
  let arr = [];
  for (let i = 0; i < list.length; i++) {
    value = list[i]
    let read_fakes = await db.collection('word')
      .aggregate()
      .sample({
        size: 4
      })
      .end()
    let tempFakes = read_fakes.list;
    let fakes = [];
    for (let ii = 0; ii < tempFakes.length; ii++) {
      //if (!(tempFakes[ii].vc_vocabulary.indexOf(" ") >= 0 || tempFakes[ii].vc_vocabulary.indexOf(".") >= 0 || tempFakes[ii].vc_vocabulary.indexOf("'") >= 0)) 
      //if (fakes.length < 3) {
      //}
      if (tempFakes[ii].vc_vocabulary != value.vc_vocabulary && fakes.length < 3) {
        fakes.push(tempFakes[ii]);
      }
    }
    for (let jj = 0; jj < 6; jj++) {
      var options = fakes.concat(value)
      options = shuffle(options);
      let o = {};
      let v = {}
      options.forEach((item, index) => {
        o[ops[index]] = item
        if (item.vc_interpretation == value.vc_interpretation) {
          v.true = ops[index]
        }
      })
      v.question = value
      v.type = 1
      v.scores = 10
      v.checked = false
      v.display_type = jj
      v.option = o
      arr.push(v)
    }
  }

  return {
    total: total,
    list: arr,
  }
}

async function get_sentence_q(event, _openid) {
  // 接收发送过来的单词
  // 生成题目
  let ids = event.ids;
  let _ = db.command;
  let read_list = await db.collection('sentence').where({
    _id: _.in(ids)
  }).get()
  let list = read_list.data;
  let total = list.length;
  // 取total*3个单词 用来混入正确答案中
  let ops = ['A', 'B', 'C', 'D']
  let arr = [];
  for (let i = 0; i < list.length; i++) {
    let read_fakes = await db.collection('sentence')
      .aggregate()
      .sample({
        size: 3
      })
      .end()
    value = list[i]
    let fakes = read_fakes.list;
    for (let j = 0; j < 6; j++) {
      var options = fakes.concat(value)
      options = shuffle(options);
      let o = {};
      let v = {}
      options.forEach((item, index) => {
        o[ops[index]] = item
        if (item._id == value._id) {
          v.true = ops[index]
        }
      })
      v.question = value
      v.type = 1
      v.scores = 10
      v.checked = false
      v.display_type = j
      v.option = o
      arr.push(v)
    }
  }
  return {
    total: total,
    list: arr,
  }
}

async function get_sg_q(event, _openid) {
  let _ = db.command;
  let read_list = await db.collection('sentence').where({
    _id: _.in(event.ids)
  }).get()
  let list = read_list.data;
  let newList = [];
  for (let i = 0; i < list.length; i++) {
    let read_fakes = await db.collection('word')
      .aggregate()
      .sample({
        size: 5
      })
      .end()
    let fakes = read_fakes.list;
    let tempVc = [];
    for (let j = 0; j < fakes.length; j++) {
      tempVc.push(fakes[j].vc_vocabulary)
    }
    let senTemp = list[i].sentence_en.split(/[\s+,+.+?]/g);
    senTemp = senTemp.filter(function (s) {
      return s && s.trim();
    })
    tempVc = tempVc.concat(senTemp);
    let newArr = [];
    for (var x = 0; x < tempVc.length; x++) {
      if (newArr.indexOf(tempVc[x]) == -1) {
        newArr.push(tempVc[x])
      }
    }
    let tempSV = list[i];
    tempSV.word = shuffle(newArr);
    tempSV.wordTrue = senTemp;
    newList.push(tempSV);
  }
  return {
    list: newList
  }
}

function shuffle(arr) {
  var l = arr.length
  var index, temp
  while (l > 0) {
    index = Math.floor(Math.random() * l)
    temp = arr[l - 1]
    arr[l - 1] = arr[index]
    arr[index] = temp
    l--
  }
  return arr
}
