const app = getApp(); 
let token = wx.getStorageSync('Authorization')

/**
 * @param {*} url 请求路径
 * @param {*} options 请求参数
 * @param {*} isNeedTocken 是否需要Tocken
 * @param {*} isJson 是否已json形式传给后台, 否则仪表单的形式。
 */
const request = (url, options,isNeedTocken,isJson) => {
   return new Promise((resolve, reject) => {
       wx.request({
           url: `${app.globalData.host}${url}`,//拼接请求路径，读取app.js中的全局变量
           method: options.method,
           data:isJson?JSON.stringify(options.data):options.data ,
           header: {
               'Content-Type':isJson?'application/json':'application/x-www-form-urlencoded',
               'Authorization': isNeedTocken?token:''  // 看自己是否需要
           },
           success(request) {
               if (request.data.code === 200) {//请求成功的状态码。看每个公司的情况。我习惯使用200来定义成功状态。
                   resolve(request.data)
               } else {
                   reject(request.data)
               }
           },
           fail(error) {
               reject(error.data)
           }
       })
   })
}

/**
*封装get请求
*/
const get = (url, options = {},isNeedTocken = true,isJson = false) => {
   return request(url, { method: 'GET', data: options },isNeedTocken,isJson)
}

/**
* 封装post请求
*/
const post = (url, options,isNeedTocken=true,isJson = false) => {
   return request(url, { method: 'POST', data: options },isNeedTocken,isJson)
}

//暴露接口
module.exports = {
   get,
   post
}
