// components/show-modal/index.js
let app = getApp();
/****
 * 功能名称：自定义弹框 (支持三个按钮)
 * show:false @description 默认false，true弹框展示
 * custom:false @description 默认false，true弹框内容自定义
 * title:'' @description 弹框内容文案
 * btns:[] @description 弹框按钮文案，最多支持三个
 * cntMarginTop:550 @description 弹框距离顶部的距离rpx
 * btnTextColor:[] @description 弹框按钮字体颜色，最多支持三个
 * tapTitle:""   顶部标题
 * tabTitleShow:false   顶部标题是否展示
 * binddidClickBtn="onClickModalBtn" 外面通过e.detail获取，值为按钮所在位置的索引 0/1/2
 * onClickModalBtn(e) {
    console.log(e.detail)
  }
 * 
 * 
 * 案例 
 * <show-modal show="{{canShow}}" title="这是一个弹框" btns="{{['取消','确定']}}" binddidClickBtn="onClickModalBtn"></show-modal>
 * 
 * 案例 custom:true
  <show-modal show="{{canShow}}" custom="{{true}}" btns="{{['取消','确定']}}" binddidClickBtn="onClickModalBtn">
    <view>这是一个弹框</view>
  </show-modal>
 */
Component({
  // 属性
  properties: {
    // 弹层开关
    show: {
      type: Boolean, //目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
      value: false, // 默认值
      observer(newVal, oldVal) {
        if (newVal) {
          this.createMaskAnimate(true);
        } else {
          this.createMaskAnimate(false);
        }
      }
    },
    // 是否内容自定义
    custom: {
      type: Boolean,
      value: false
    },
    // 弹框内容
    title: {
      type: String,
      value: "这是一个弹框",
    },
    // 按钮文字内容
    btns: {
      type: Array,
      value: ['取消', '确定'] // 默认值
    },
    // 弹框距离顶部距离
    cntMarginTop: {
      type: Number,
      value: 550 // 默认值
    },
    // 按钮文字颜色
    btnTextColor: {
      type: Array,
      value: ['#A2A2A8', '#fff', '#303030'], // 默认值
      observer(newVal, oldVal) {
        if (!newVal && !newVal.length && newVal.length != 3) {
          this._btnTextColor = ['#A2A2A8', '#fff', '#303030'];
        } else {
          this._btnTextColor = newVal;
        }
      }
    },
    //标题是否显示
    tabTitleShow:{
      type:Boolean,
      value:true
    },
    // 标题
    tapTitle: {
      type: String,
      value: '提示信息'
    }
  },
  // 初始化数据
  data: {
    _show: false,
    _btnTextColor: ['#A2A2A8', '#fff', '#303030'],
    maskAnimate: [],
    frameAnimate: [],
    title: "",         // 提示弹框
    success: () => { },    // 成功
    error: () => { }          // 失败
  },
  lifetimes: {
    attached: function() {
      function success ()  { };    // 成功
      function error()  { } 
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  methods: {
    selectMask: function (e) {
      // this.setData({
      //   show: !this.data.show
      // });
    },
    // 选中底部的按钮
    selectBtn: function (e) {
      let index = e.currentTarget.dataset.index;
      const that = this
      this.triggerEvent('didClickBtn', index); // 外面通过e.detail获取index的值
      // if (index == 0) {
      //   this.setData({
      //     show: false
      //   }, () => {
      //     that.data.error()
      //   })
      // } else {
      //   this.setData({
      //     show: false
      //   }, () => {
      //     that.data.success()
      //   })
      // }

    },
    move: function () {
      return;
    },
    showModal: function (params) {
      let {success,error} = this
      this.setData({
        show: !this.data.show,
        title: params.title,
        tabTitleShow:params.tabTitleShow,
        success: params.success || function () { },
        error: params.error || function () { },
      })
    },
    //动画-遮罩层淡入淡出
    createMaskAnimate(currentStatu) {
      if (currentStatu) {
        this.setData({
          _show: currentStatu
        })
        this.createframeAnimate(true);
      } else {
        setTimeout(() => {
          this.setData({
            _show: false
          })
        }, 300)
        this.createframeAnimate(false);
      }
      let maskAnimate = wx.createAnimation({
        duration: 300,
        timingFunction: 'ease-in-out',
        delay: 0
      })
      this.maskAnimate = maskAnimate;
      maskAnimate.opacity(currentStatu ? 0.7 : 0).step();
      setTimeout(() => {
        this.setData({
          maskAnimate: maskAnimate.export()
        });
      }, 50)
    },
    //动画-弹层内容Q弹
    createframeAnimate(currentStatu) {
      let frameAnimate = wx.createAnimation({
        duration: 150,
        timingFunction: 'ease',
        delay: 0
      })
      this.frameAnimate = frameAnimate;
      frameAnimate.opacity(currentStatu ? 0.5 : 1).scale(currentStatu ? 1.05 : 1).step();
      this.setData({
        frameAnimate: frameAnimate.export()
      });
      setTimeout(() => {
        // 执行第二组动画 
        frameAnimate.opacity(currentStatu ? 1 : 0).scale(currentStatu ? 1 : 0.9).step();
        // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象 
        this.setData({
          frameAnimate: frameAnimate.export()
        })
      }, 150)
    }
  }
})