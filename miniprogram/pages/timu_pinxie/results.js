// pages/results/results.js
var app = getApp();
Page({
  data: {
    totalScore: 0, // 分数
    wrongList: [], // 错误的题数-乱序
    wrongListSort: [], // 错误的题数-正序
    chooseValue: [], // 选择的答案
    remark: ["恭喜过关", "恭喜过关", "得分没有超过80，继续努力哦！"], // 评语
    modalShow: false
  },
  onLoad: function (options) {
    this.setData({
      location_id: options.location_id
    })

    // wx.setNavigationBarTitle({ title: options.testId }) // 动态设置导航条标题

    let wrongList = JSON.parse(options.wrongList);
    let wrongListSort = JSON.parse(options.wrongListSort);
    let chooseValue = JSON.parse(options.chooseValue);
    let questionList = JSON.parse(options.questionList)


    this.setData({
      totalScore: options.totalScore != "" ? options.totalScore : "无",
      wrongList: wrongList,
      wrongListSort: wrongListSort,
      chooseValue: chooseValue,
      questionList: questionList, // 拿到答题数据

    })
    console.log(this.data.chooseValue);

    // 如果超过80分 更新用户数据 answer 字段  为对象
    // if (this.data.totalScore >= 80) {
    //   wx.cloud.callFunction({
    //     name: 'api',
    //     data: {
    //       fn: 'update_user_answer',
    //       location_id: this.data.location_id
    //     }
    //   }).then(res => {
    //     console.log(res)
    //   })
    // }



  },
  // 查看错题
  toView: function () {
    // 显示弹窗
    this.setData({
      modalShow: true
    })
  },
  // 返回首页
  toIndex: function () {
    wx.reLaunch({
      url: '../index/index'
    })
  }
})