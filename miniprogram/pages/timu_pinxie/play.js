var app = getApp();
var timerId;
var outId;
import req from "../../api/req";
var requestUrl ={
  spellPracticeList:"/kl/word/spellPracticeList"
}
Page({
  data: {
    modalShow: false,
    modelTapTitle: '',
    modelTitle: '',
    index: 0, // 题目序列
    chooseValue: [], // 选择的答案序列
    totalScore: 100, // 总分
    wrong: 0, // 错误的题目数量
    wrongList: [], // 错误的题目集合-乱序
    wrongListSort: [], // 错误的题目集合-正序
    page: 1, //关卡
    num: 10,
    show_style: false,
    second: 10,
  },
  insert() {
    let text = this.data.text || ''
    this.setData({
      text: text + "'"
    })
  },
  AudioRead(word, type) {
    if (this.audio) {
      this.audio.stop()
    }
    this.audio = wx.createInnerAudioContext({
      useWebAudioImplement:true
    })
    this.audio.src = "http://dict.youdao.com/dictvoice?audio=" + word + '&type=' + type
    this.audio.play()
    this.audio.onPlay((res) => {
      wx.hideLoading();
    })
    this.audio.onError((err) => {
      if (type == 2) {
        this.AudioRead(word, 0);
      }
      // if (type == 1) {
      //   wx.showToast({
      //     title: '失败',
      //     icon: "error",
      //     duration: 2000,
      //   })
      // }
    })
    // var audio = wx.createInnerAudioContext()
    // if(wx.setInnerAudioOption){
    //   wx.setInnerAudioOption({
    //     autoplay: true
    //   })
    // }else{
    //   audio.autoplay = true;
    // }
    // let src = "http://dict.youdao.com/dictvoice?audio=" + word + "&type="+type;
    // audio.src = src;
    // audio.autoplay = true;
    // audio.play();
  },
  play_audio(e) {
    let word = e.currentTarget.dataset.word;
    let type = e.currentTarget.dataset.type || '1';
    // this.AudioRead(word,2);
    if (this.audio) {
      this.audio.stop()
    }
    this.audio = wx.createInnerAudioContext({
      useWebAudioImplement:true
    })
    // this.audio.src = "http://dict.youdao.com/dictvoice?audio=" + word// + '&type=' + 2
        this.audio.src = word// + '&type=' + 2

    this.audio.play()
  },
  input(e) {
    this.setData({
      text: e.detail.value
    })
  },
  check() {
    let q = this.data.questionList[this.data.shuffleIndex[this.data.index]]
    console.log('q', q);
    if (!this.data.text) {
      wx.showToast({
        title: '请输入单词',
        icon: 'none'
      })
      return false
    }
    if (q.answer == this.data.text) {
      // wx.showToast({
      //   title: '恭喜，回答正确',
      // })
      return true
    } else {
      wx.showModal({
        title:'错误，正确答案为',
        content: q.answer,
      })
      // wx.showToast({
      //   title: '错误，正确答案为' + q.vc_vocabulary,
      //   icon: 'none'
      // })
      return false
    }
  },
  onLoad: function (options) {
    wx.showLoading({
      title: '正在加载',
    })
    // console.log(options);
    // if (options.page) {
    //   this.setData({
    //     page: options.page,
    //     location_id: options.location_id
    //   })
    // }


    const eventChannel = this.getOpenerEventChannel()
    eventChannel['on'] && eventChannel.on('send', data => {
      let arr = [];
      for (let i = 0; i < data.data.length; i++) {
        // data.data[i].length
        for (let j = 0; j < data.data[i].length; j++) {
          arr.push(data.data[i][j].id)
        }
      }
      this.setData({
        all_list: arr
      })
      wx.showLoading();
      req.post(
        requestUrl.spellPracticeList, arr,
        true, true
      ).then(res => {
        console.log('res', res);
        if (res.code == 200) {
          let arr = []
        // res.data.forEach(v => {
        //   // for (let i = 0; i < 2; i++) {
        //   //   let tmp = JSON.stringify(v)
        //   //   v = JSON.parse(tmp)
        //   //   v.display_type = i
        //   //   arr.push(v)
        //   // }
        //   for (let i = 0; i < 2; i++) {
        //     console.log('ss');
        //     let tmp = JSON.stringify(v)
        //     console.log('temp', temp);
        //     v = JSON.parse(tmp)
        //     v.display_type = i
        //     arr.push(v)
        //   }
        // })
        this.setData({
          questionList: res.data, // 拿到答题数据
        })


        let count = this.generateArray(0, this.data.questionList.length - 1); // 生成题序
        this.setData({
          shuffleIndex: this.shuffle(count).slice(0, this.data.questionList.length) // 
        })
    
          wx.hideLoading()
      
        }
      }).catch(err => {
        if (err.code === 312) {
          wx.showModal({
            title: '提示',
            content: err.message,
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定')
                wx.navigateTo({
                  url: '../member/member',
                })
                // wx.navigateBack({
                //   delta: 0,
                // })
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        //请求报错，或者状态码返回错误。
        wx.hideLoading()
      })

      // wx.cloud.callFunction({
      //   name: 'api',
      //   data: {
      //     fn: 'get_q_pinxie',
      //     ids: arr
      //   }
      // }).then(res => {
      //   wx.hideLoading({
      //     success: (res) => { },
      //   })
        
        
      })

    wx.setNavigationBarTitle({
      title: '单词拼写'
    }) // 动态设置导航条标题



  },

  /*
   * 数组乱序/洗牌
   */
  shuffle: function (arr) {
    let i = arr.length;
    while (i) {
      let j = Math.floor(Math.random() * i--);
      [arr[j], arr[i]] = [arr[i], arr[j]];
    }
    return arr;
  },
  /*
   * 单选事件
   */
  radioChange: function (e) {
    let history = this.data.history || {}
    let is_right = this.chooseError(e.detail.value)
    history[this.data.index] = {
      ck: e.detail.value,
      is_right
    }
    this.setData({
      history
    })


    console.log('checkbox发生change事件，携带value值为：', e.detail.value)
    this.data.chooseValue[this.data.index] = e.detail.value;
    
    console.log(this.data.chooseValue);
  },
  /*
   * 多选事件
   */
  checkboxChange: function (e) {
    console.log('checkbox发生change事件，携带value值为：', e.detail.value)
    this.data.chooseValue[this.data.index] = e.detail.value.sort();
    console.log(this.data.chooseValue);
  },
  /*
   * 退出答题 按钮
   */
  outTest: function () {
    wx.showModal({
      title: '提示',
      content: '你真的要退出答题吗？',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
          wx.navigateBack({
            delta: 0,
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  /*
   * 下一题/提交 按钮
   */
  preSubmit() {
    // 渲染上一题
    this.setData({
      index: this.data.index - 1
    })


  },
  nextSubmit: function () {
    if (!this.data.text) {
      wx.showToast({
        title: '请输入答案',
        icon: 'none'
      })


      return
    }
    let right = this.check()
    console.log('re', right);
    if (right) {
      let countNum = this.data.shuffleIndex.length;
      if (this.data.index == countNum - 2) {
        let gradeId = wx.getStorageSync('gradeId')
        let gradeNum = wx.getStorageSync('gradeNum')
        if (gradeId) {
          gradeNum += this.data.shuffleIndex.length
          wx.cloud.callFunction({
            name: 'api', data: {
              fn: 'update_grade',
              data: {
                id: gradeId,
                grade: gradeNum
              }
            }
          }).then(res => {
            wx.setStorageSync('gradeNum', gradeNum)
          })
        }
      }
      // setTimeout(() => {
      if (this.data.index < this.data.shuffleIndex.length - 1) {
        // 渲染下一题
        this.setData({
          index: this.data.index + 1,
          text: ''
        })
      } else {
        // wx.showToast({
        //   icon: 'none',
        //   title: '已是最后一道题',
        // })

        wx.showModal({
          title: '提示',
          content: '确认再来一次',
          confirmColor: "#ff0000",
          success: res => {
            if (res.confirm) {
              this.setData({
                index: 0,
                text: ''
              })
            }
          }
        })
      }
      // }, 1000)
    }

  },
  /*
   * 错题处理
   */
  chooseError2: function () {
    var trueValue = this.data.questionList[this.data.shuffleIndex[this.data.index]]['true'];
    var chooseVal = this.data.chooseValue[this.data.index];
    console.log('选择了' + chooseVal + '答案是' + trueValue);



    if (chooseVal.toString() != trueValue.toString()) {
      console.log('错了');
      this.data.wrong++;
      // 已经有了就别push了
      let have = false;
      this.data.wrongListSort.forEach(v => {
        if (this.data.index == v) {
          have = true
        }
      })
      if (!have) {
        this.data.wrongListSort.push(this.data.index);
        this.data.wrongList.push(this.data.shuffleIndex[this.data.index]);
      }



      // this.setData({
      //   totalScore: this.data.totalScore - this.data.questionList[this.data.shuffleIndex[this.data.index]]['scores'] // 扣分操作
      // })
    }
  },
  wrong() {
    let t = 0;
    for (let key in this.data.history) {
      if (!this.data.history[key].is_right) {
        t += 1
      }
    }
    return t;
  },
  totalScore() {
    let t = 0;
    for (let key in this.data.history) {
      if (this.data.history[key].is_right) {
        t += 10
      }
    }
    return t;
  },
  chooseError: function (chooseVal) {
    var trueValue = this.data.questionList[this.data.shuffleIndex[this.data.index]]['true'];
    // var chooseVal = this.data.chooseValue[this.data.index];
    console.log('选择了' + chooseVal + '答案是' + trueValue);
    this.setData({
      show_style: true
    })
    timerId = setInterval(() => {
      this.setData({
        second: this.data.second - 1
      })
    }, 1000)
    // 10秒后自动跳转下一题
    outId = setTimeout(this.nextSubmit, 10000)


    return chooseVal.toString() == trueValue.toString()
    if (chooseVal.toString() != trueValue.toString()) {
      console.log('错了');
      // this.data.wrong++;
      // this.data.wrongListSort.push(this.data.index);
      // this.data.wrongList.push(this.data.shuffleIndex[this.data.index]);
      // this.setData({
      //   totalScore: this.data.totalScore - this.data.questionList[this.data.shuffleIndex[this.data.index]]['scores'] // 扣分操作
      // })
    }
  },
  /**
   * 生成一个从 start 到 end 的连续数组
   * @param start
   * @param end
   */
  generateArray: function (start, end) {
    return Array.from(new Array(end + 1).keys()).slice(start)
  },
  onunload() {
    clearInterval(timerId)
    clearTimeout(outId)
  }
})