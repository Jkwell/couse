import req from "../../api/req";
var requestUrl = {
  zidingyi: "/kl/word/list"
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [

    ]
  },
  clear(){
    this.setData({
      words:[]
    })
  },
  go() {
    let list = wx.getStorageSync('zidingyi')
    if (list.length) {
      if (list.length < 4) {
        wx.showToast({
          title: '请至少添加四个单词',
          icon: 'none'
        })
      } else {
        wx.navigateTo({
          url: '../zidingyi/choose',
        })
      }
    }  else {
      wx.showToast({
        title: '请添加单词',
        icon: 'none'
      })
    }
  },
  select(e) {
    let list = this.data.list
    list = list.map((v, i) => {
      if (i == this.data.index) {
        v = e.currentTarget.dataset.value
      }
      return v
    })
    this.setData({
      list
    })
    wx.setStorageSync('zidingyi', list)
  },
  input(e) {
    if (!e.detail.value) {
      return;
    }
    wx.showLoading();
    req.get(
      requestUrl.zidingyi, {
        vcVocabulary: e.detail.value,
        // bookId: '1ace8ef160aa1b220143e32543de8949',
        pageNo: 1,
        pageSize: 99999
      },
      true, false
    ).then((res) => {
      if (res.code == 200) {
        this.setData({
          words: res.data.records
        })
        wx.hideLoading()
    
      }
    }).catch(err => {
      //请求报错，或者状态码返回错误。
      wx.hideLoading()
    })
    // const db = wx.cloud.database();
    // // 读取列表
    // let _ = db.command;
    // let where = _.or([{
    //   vc_vocabulary: db.RegExp({
    //     regexp: e.detail.value,
    //     options: 'i',
    //   })
    // }])
    // let where = {
    //   'vcVocabulary': e.detail.value
    // }
    // db.collection('word')
    //   .where(where)
    //   .limit(100)
    //   .get()
    //   .then(res => {
    //     console.log('res', res);
    //     wx.hideLoading()
    //     if (res.data.length) {
    //       this.setData({
    //         words: res.data
    //       })
    //     }
    //   })
    let index = e.currentTarget.dataset.index
    this.setData({
      index,
    })
    // let list = this.data.list.map((v, i) => {
    //   if (i == index) {
    //     v.vc_vocabulary = e.detail.value
    //   }
    //   return v
    // })
    // wx.setStorageSync('zidingyi', list)
  },
  add(e) {
    let list = this.data.list || []
    list.push({})
    this.setData({
      list,
    })
    wx.setStorageSync('zidingyi', list)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let list = wx.getStorageSync('zidingyi')
    this.setData({
      list,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})