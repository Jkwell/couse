// pages/product_list/product_list.js
Page({

  flip(e) {
    let _id = e.currentTarget.dataset.v._id
    let arr = wx.getStorageSync('zidingyi') || []
    let list = this.data.list.map(v => {
      if (v._id == _id) {
        v.flipped = !v.flipped
        arr.push(v)
      }
      return v
    })
    wx.setStorageSync('zidingyi', arr)
    this.setData({
      list,
      zidingyi: arr
    })
  },
  go() {
    let list = wx.getStorageSync('zidingyi')
    if (list.length) {
      // data = list.filter(v=>v.flipped)
      // wx.setStorageSync('data', data)
      wx.navigateTo({
        url: '../zidingyi/choose',
      })
    } else {
      wx.showToast({
        title: '请点击选择单词',
        icon: 'none'
      })
    }

  },


  /**
   * 页面的初始数据
   */
  data: {
    t: 2,
    inputShowed: false,
    inputVal: "",
    where: {
      // is_in_stock: true
    },
    list: [],
    page: 1,
    limit: 20,
    total: 0,
    collection: 'word',
  },
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  hideInput: function () {
    this.setData({
      inputVal: "",
      inputShowed: false
    });
    this.setData({
      page: 1,
      list: [],
      where: {
        // is_in_stock: this.data.where.is_in_stock
      }
    })
    // this.get_list()
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
    this.setData({
      page: 1,
      list: [],
      where: {
        // is_in_stock: this.data.where.is_in_stock
      }
    })
    // this.get_list()
  },
  sou() {
    const db = wx.cloud.database()
    let _ = db.command;
    this.setData({
      page: 1,
      list: [],
      // where: _.or([{
      //   vc_vocabulary: db.RegExp({
      //     regexp: this.data.inputVal,
      //     options: 'i',
      //   })
      // }, {
      //   vc_interpretation: db.RegExp({
      //     regexp: this.data.inputVal,
      //     options: 'i',
      //   })
      // }])
      // 'where.headWord':this.data.inputVal
      'where.vc_vocabulary': this.data.inputVal
    })
    this.get_list()
  },
  inputTyping: function (e) {

    this.setData({
      inputVal: e.detail.value
    });
    return;
    const db = wx.cloud.database()
    let _ = db.command;
    this.setData({
      page: 1,
      list: [],
      // where: _.or([{
      //   vc_vocabulary: db.RegExp({
      //     regexp: this.data.inputVal,
      //     options: 'i',
      //   })
      // }, {
      //   vc_interpretation: db.RegExp({
      //     regexp: this.data.inputVal,
      //     options: 'i',
      //   })
      // }])
      // 'where.headWord':this.data.inputVal
      'where.vc_vocabulary': this.data.inputVal
    })
    this.get_list()
  },

  down(e) {
    let _id = e.currentTarget.dataset.id;
    wx.showLoading();
    wx.cloud.callFunction({
      name: 'eval',
      data: {
        fn: 'update_product',
        _id: _id,
        data: {
          is_in_stock: false
        }
      }
    }).then(res => {
      wx.hideLoading();
      wx.showToast({
        title: '下架成功',
      })
      let list = this.data.list;
      list = list.filter(v => v._id != _id)
      this.setData({
        list: list
      })
    })
  },

  up(e) {
    let _id = e.currentTarget.dataset.id;
    wx.showLoading();
    wx.cloud.callFunction({
      name: 'eval',
      data: {
        fn: 'update_product',
        _id: _id,
        data: {
          is_in_stock: true
        }
      }
    }).then(res => {
      wx.hideLoading();
      wx.showToast({
        title: '上架成功',
      })
      let list = this.data.list;
      list = list.filter(v => v._id != _id)
      this.setData({
        list: list
      })
    })
  },
  set(e) {
    let item = e.currentTarget.dataset.item;

    this.setData({
      'where.is_in_stock': Boolean(item),
      page: 1,
      nomore: false,
      list: []
    })
    this.get_list();
  },

  get_list() {
    wx.showLoading();
    const db = wx.cloud.database();
    this.setData({
      list: []
    })
    // 读取总数
    db.collection(this.data.collection)
      .where(this.data.where)
      .count()
      .then(res => {
        this.setData({
          total: res.total
        })
      })
    // 读取列表
    db.collection(this.data.collection)
      .where(this.data.where)
      .orderBy('created_at', 'desc')
      .skip((this.data.page - 1) * this.data.limit)
      .limit(this.data.limit)
      // .field({
      //   name: true,
      //   main_image: true,
      //   price: true,
      //   is_in_stock: true,
      // })
      .get()
      .then(res => {
        wx.hideLoading()
        if (res.data.length) {
          this.setData({
            list: this.data.list.concat(res.data)
          })
        }
      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setStorageSync('zidingyi', [])
    // let zidingyi = wx.getStorageSync('zidingyi')
    // this.setData({
    //   zidingyi,
    // })
    // this.get_list();
    // wx.cloud.callFunction({
    //   name: 'api',
    //   data: {
    //     fn: 'get_openid'
    //   }
    // }).then(res => {
    //   this.setData({
    //     openid: res.result
    //   })
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.inputShowed) {
      if (this.data.list.length == this.data.total) {
        this.setData({
          nomore: true
        })
        return;
      }
      this.setData({
        page: this.data.page + 1
      })
      this.get_list();
    }

  }

})