var app = getApp();
var timerId;
var outId;
import req from "../../api/req";
var requestUrl = {
  sentenceSpellPracticeList: "/kl/sentence/sentenceSpellPracticeList"
}
Page({
  data: {
    index: 0, // 题目序列
    chooseValue: [], // 选择的答案序列
    show_style: false,
    second: 10,
  },
  clear(e) {
    this.setData({
      chooseValue: []
    })
  },
  choose(e) {
    let tempC = this.data.chooseValue;
    tempC.push(e.currentTarget.dataset.word);
    this.setData({
      chooseValue: tempC
    })
  },
  play_audio(e) {
    console.log('e', e);
    let word = e.currentTarget.dataset.word;
    if (this.audio) {
      this.audio.stop()
    }
    this.audio = wx.createInnerAudioContext({
      useWebAudioImplement: true
    })
    // let tempWord = word.replace(/[\s+,+.+?+!+'+"]/g,'')
    this.audio.src = word
    // this.audio.src = "http://dict.youdao.com/dictvoice?audio=" + word
    this.audio.play()
  },
  input(e) {
    this.setData({
      text: e.detail.value
    })
  },
  check() {
    let tempC = this.data.chooseValue
    let q = this.data.questionList[this.data.shuffleIndex[this.data.index]]
    console.log('q', q);
    if (!tempC.length) {
      wx.showToast({
        title: '请选择单词',
        icon: 'none'
      })
      return false
    }
    let tempStatus = false;
    if (tempC.length == q.answerArray.length) {
      for (let i = 0; i < tempC.length; i++) {
        if (tempC[i] == q.answerArray[i]) {
          tempStatus = true;
        } else {
          tempStatus = false;
          break
        }
      }
    }
    if (tempStatus) {
      wx.showToast({
        title: '恭喜，回答正确',
      })
      return true
    } else {
      wx.showModal({
        title:'错误，正确答案为',
        content: q.answer,
      })
      // wx.showToast({
      //   title: 'title',
      //   icon: 'none'
      // })
      this.setData({
        chooseValue: []
      })
      return false
    }
  },
  onLoad: function (options) {
    wx.showLoading({
      title: '正在加载',
    })
    const eventChannel = this.getOpenerEventChannel()
    eventChannel['on'] && eventChannel.on('send', data => {
      let arr = [];
      for (let i = 0; i < data.data.length; i++) {
        // data.data[i].length
        for (let j = 0; j < data.data[i].length; j++) {
          arr.push(data.data[i][j].id)
        }
      }
      this.setData({
        all_list: arr
      })
      req.post(
        requestUrl.sentenceSpellPracticeList, arr,
        true, true
      ).then(res => {
        let arrT = []
        // res.result.list.forEach(v => {
        //   for (let ii = 0; ii < 2; ii++) {
        //     let tmp = JSON.stringify(v);
        //     tmp = JSON.parse(tmp);
        //     tmp.display_type = ii
        //     arrT.push(tmp)
        //   }
        // })
        this.setData({
          questionList: res.data, // 拿到答题数据
        })
        let count = this.generateArray(0, this.data.questionList.length - 1); // 生成题序
        this.setData({
          shuffleIndex: this.shuffle(count).slice(0, this.data.questionList.length) // 
        })
      }).catch((err) => {
        console.log('err',  err);
        if (err.code === 312) {
          wx.showModal({
            title: '提示',
            content: err.message,
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定')
                wx.navigateTo({
                  url: '../member/member',
                })
                // wx.navigateBack({
                //   delta: 0,
                // })
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      })
      wx.setNavigationBarTitle({
        title: '组句练习'
      }) // 动态设置导航条标题
      wx.hideLoading({
        success: (res) => { },
      })

    })
  },

  /*
   * 数组乱序/洗牌
   */
  shuffle: function (arr) {
    let i = arr.length;
    while (i) {
      let j = Math.floor(Math.random() * i--);
      [arr[j], arr[i]] = [arr[i], arr[j]];
    }
    return arr;
  },
  nextSubmit: function () {
    if (!this.data.chooseValue.length) {
      wx.showToast({
        title: '请选择答案',
        icon: 'none'
      })
      return
    }
    let right = this.check()
    if (right) {
      let countNum = this.data.shuffleIndex.length;
      if (this.data.index == countNum - 2) {
        let gradeId = wx.getStorageSync('gradeId')
        let gradeNum = wx.getStorageSync('gradeNum')
        if (gradeId) {
          gradeNum += this.data.shuffleIndex.length
          wx.cloud.callFunction({
            name: 'api', data: {
              fn: 'update_grade',
              data: {
                id: gradeId,
                grade: gradeNum
              }
            }
          }).then(res => {
            wx.setStorageSync('gradeNum', gradeNum)
          })
        }
      }
      // setTimeout(() => {
      if (this.data.index < this.data.shuffleIndex.length - 1) {
        // 渲染下一题
        this.setData({
          index: this.data.index + 1,
          chooseValue: []
        })
      } else {
        wx.showModal({
          title: '提示',
          content: '确认再来一次',
          confirmColor: "#ff0000",
          success: res => {
            if (res.confirm) {
              this.setData({
                index: 0,
                chooseValue: []
              })
            }
          }
        })
      }
    }

  },
  /**
   * 生成一个从 start 到 end 的连续数组
   * @param start
   * @param end
   */
  generateArray: function (start, end) {
    return Array.from(new Array(end + 1).keys()).slice(start)
  },
  onunload() {
    clearInterval(timerId)
    clearTimeout(outId)
  }
})