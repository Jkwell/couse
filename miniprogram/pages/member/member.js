// pages/member/member.js
import req from "../../api/req";
var requestUrl ={
  user:"/kl/user/get",
  product:'/kl/product'
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    myurl: '',
    modalHidden: true,
    userInfo: null,
    vip: [
  ],
  currentIndex: 0
  },

  changeVip(e) {
    this.setData({
      currentIndex: e.currentTarget.dataset.index
    })
  },

  /**
   * 显示弹窗
   */
  modalConfirm: function() {
    this.setData({
      modalHidden: true
    })
  },

  /**
   * 点击取消
   */
  modalCancel: function() {
    // do something
    this.setData({
      modalHidden: true
    })
  },


  showCard() {
    this.setData({
      modalHidden: false
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let userinfo = wx.getStorageSync('userInfo');
    if (userinfo) {
      this.setData({
        islogin: true
      })
    }
    wx.setNavigationBarTitle({
      title: '会员中心'
    })
    wx.cloud.callFunction({ name: 'api', data: { fn: 'get_user' } }).then(res => {
      let info = res.result;
      if (info) {
        let date = new Date(info.data[0].created_date)
        this.setData({
          myurl: info.data[0].avatarUrl,
          myname: info.data[0].nickName,
          create_date: date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日'
        })
      }
    })
    req.get(
      requestUrl.user, null,
      true, false
    ).then((res) => {
      this.setData({
        userInfo: res.data.record
      })
      console.log('res', res);
    })

    req.get(
      requestUrl.product, null,
      true, false
    ).then((res) => {
      this.setData({
        vip: res.data.records
      })
      console.log('res2', res);
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})