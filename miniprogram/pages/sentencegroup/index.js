const db = wx.cloud.database();
import req from "../../api/req";
var requestUrl ={
  sentenceList:"/kl/sentence/list"
}
Page({
  data: {
    modalShow: false,
    modelTapTitle: '',
    modelTitle: '',
    limit: 100,
    page: 1,
    total: 0,
    list: [],
    choose: {},
  },

  go() {
    // 把单词全部记录到本地
    let arr2 = []
    let tempCho = this.data.choose
    for (let k in this.data.choose) {
      if (this.data.choose[k]) {
        arr2.push(this.data.display[k])
      }
    }
    if (arr2.length) {
      wx.navigateTo({
        url: '../sentencegroup_e/exercises',
        success: function (res) {
          res.eventChannel.emit('send', {
            data: arr2
          })
        }
      })
    } else {
      wx.showModal({
        title: '请选择句子'
      })
    }
  },
  select(e) {
    let choose = this.data.choose
    if (!choose[e.currentTarget.dataset.index]) {
      choose[e.currentTarget.dataset.index] = true
    } else {
      choose[e.currentTarget.dataset.index] = false
    }

    this.setData({
      choose,
    })
  },

  onClickModalBtn(e) {
    let word = this.data.modelTapTitle;
    if (e.detail == 0) {
      if (this.audio) {
        this.audio.stop()
      }
      this.audio = wx.createInnerAudioContext({
        useWebAudioImplement: true
      })
      let tempWord = word.replace(/[\s+,+.+?+!+'+"]/g,'')
      this.audio.src = "cloud://cloud1-7gva9b3w58c3dda0.636c-cloud1-7gva9b3w58c3dda0-1307253701/sentence/" + tempWord+".mp3"
      // this.audio.src = "http://dict.youdao.com/dictvoice?audio=" + word
      this.audio.play()
    }
    else {
      this.audio.stop()
      this.setData({
        modalShow: false,
      })
    }
  },
  play_audio(e) {

    let val = e.currentTarget.dataset.val
    let word = val.sentenceEn;
    let cloud = val.cloud;

    this.setData({
      modelTapTitle: word,
      modelTitle: val.sentenceZh,
      modalShow: true,
    })
    if (this.audio) {
      this.audio.stop()
    }
    this.audio = wx.createInnerAudioContext({
      useWebAudioImplement: true
    })
    let tempWord = word.replace(/[\s+,+.+?+!+'+"]/g,'')
    this.audio.src = "cloud://cloud1-7gva9b3w58c3dda0.636c-cloud1-7gva9b3w58c3dda0-1307253701/sentence/" + tempWord+".mp3"
    // this.audio.src = "http://dict.youdao.com/dictvoice?audio=" + word
    this.audio.play()
  },

  get_list() {

    wx.showLoading({
      title: '正在加载'
    })
    wx.showLoading();
    req.get(
      requestUrl.sentenceList, {
        bookId:  wx.getStorageSync('dict').id,
        // bookId: '1ace8ef160aa1b220143e64c683872fd',
        pageNo: this.data.page,
        pageSize: this.data.limit
      },
      true, false
    ).then(res => {
      if (res.code == 200) {
        let tempList = res.data.records;
      let tempListNew = [];

      this.setData({
        // list: this.data.list.concat(res.result.list),
        list: this.data.list.concat(tempList),
        total: res.data.total
      })
      let display = []
      let group = this.data.list.length / 10
      for (let i = 0; i < group; i++) {
        display.push(this.data.list.slice(i * 10, i * 10 + 10))
      }
      this.setData({
        display,
      })
  
        wx.hideLoading()
    
      }
    }).catch(err => {
      //请求报错，或者状态码返回错误。
      wx.hideLoading()
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let o = wx.getStorageSync('dict');
    let category_id = o._id || 43
    let title = o.title || '组句练习'
    wx.setNavigationBarTitle({
      title,
    })
    this.setData({
      'title': title,
      'bookId': category_id
    })
    this.get_list();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.list.length == this.data.total) {
      wx.showToast({
        title: '没有更多',
        icon: 'none'
      })
      return;
    }
    this.setData({
      page: this.data.page + 1
    })
    this.get_list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  }
})