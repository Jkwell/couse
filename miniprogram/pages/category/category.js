// pages/category/category.js
import req from "../../api/req";
var requestUrl ={
  categoryList:"/kl/categorySort/list"
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    collection: 'category_sort',
    pid: '0',
    page: 1,
    limit: 20,
    list: [],
    total: 0,
    where: {},
    history_list: {}
  },

  select(e) {
    if (this.data.pid == "0") {
      if (e.currentTarget.dataset.title == "其他") {
        this.setData({
          pid: '9.99',
          list: [],
          page: 1
        })
      } else {
        this.setData({
          pid: e.currentTarget.dataset._id,
          list: [],
          page: 1
        })
      }
      this.get_list();
    } else if (e.currentTarget.dataset.pid) {
      this.setData({
        pid: '0',
        list: [],
        page: 1
      })
      this.get_list();
    } else {
      wx.setStorageSync('dict', {
        id: e.currentTarget.dataset._id,
        title: e.currentTarget.dataset.title
      })
      wx.showToast({
        title: '选择成功',
        icon: 'success'
      })
      setTimeout(function () {
        wx.reLaunch({
          url: '../index/index',
        })
      }, 500)
    }
  },

  get_history_from_local() {
    let history_list = wx.getStorageSync('history') || {}
    let arr = []
    for (let k in history_list) {
      arr.push({
        key: k,
        value: history_list[k]
      })
    }
    arr.reverse()
    arr.splice(5)
    this.setData({
      history_list: arr
    })

  },

  go(e) {
    let _id = e.currentTarget.dataset.category_id
    let title = e.currentTarget.dataset.title
    wx.navigateTo({
      url: '../read/read?category_id=' + _id + '&title=' + title,
    })
  },
  get_list() {
    wx.showLoading();
    req.get(
      requestUrl.categoryList, {
        pid: this.data.pid,
        pageNo: this.data.page,
        pageSize: this.data.limit
      },
      true, false
    ).then(res => {
      if (res.code == 200) {
        this.setData({
          list: this.data.list.concat(res.data.records) ,
          total:res.data.total
        })
        wx.hideLoading()
        
      }
    }).catch(err => {
      //请求报错，或者状态码返回错误。
      wx.hideLoading()
    })
   

    // const db = wx.cloud.database();
    // this.setData({
    //   where: { pid: this.pid }
    // })
    // // 读取总数
    // db.collection(this.data.collection)
    //   // .where(this.data.where)
    //   .where({ 'pid': this.data.pid })
    //   .count()
    //   .then(res => {
    //     this.setData({
    //       total: res.total
    //     })
    //   })
    // // 读取列表
    // db.collection(this.data.collection)
    //   .where({ 'pid': this.data.pid })
    //   // .orderBy('created_at', 'desc')
    //   .skip((this.data.page - 1) * this.data.limit)
    //   .limit(this.data.limit)
    //   .orderBy('category_type', 'asc')
    //   .field({
    //     _id: true,
    //     id: true,
    //     original_id: true,
    //     name: true,
    //   })
    //   .get()
    //   .then(res => {
    //     wx.hideLoading()
    //     if (res.data.length) {
    //       this.setData({
    //         list: this.data.list.concat(res.data)
    //       })
    //     }
    //   })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '选择词库',
    })
    this.get_list();


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.get_history_from_local();

    // 在适合的场景显示插屏广告
    setTimeout(() => {
      if (this.interstitialAd) {
        this.interstitialAd.show().catch((err) => {
          console.error(err)
        })
      }
    }, 15000)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.list.length == this.data.total) {
      this.setData({
        nomore: true
      })
      return;
    }
    this.setData({
      page: this.data.page + 1
    })
    this.get_list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onShareTimeline() {

  }
})