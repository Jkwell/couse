
import req from "../../api/req";
var requestUrl ={
  gradeList:"/kl/grade/list",
  ownGrade:'/kl/grade/get'
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    monthOfJD: '',
    userGrade: {},
    myrank: 0,
    myurl: '',
    myname: '未注册',
    list: [],
    page: 1,
    limit: 100,
    total: 0,
    rank: 0
  },
  get_my() {
    let that = this;
    let today = new Date();
    let month = today.getMonth() + 1;
    // let dateTemp = 0;
    // if (month <= 3) {
    //   dateTemp = 1
    // } else if (month <= 6) {
    //   dateTemp = 2;
    // } else if (month <= 9) {
    //   dateTemp = 3;
    // } else {
    //   dateTemp = 4;
    // }
    req.get(
      requestUrl.ownGrade, {
        year: today.getFullYear(),
        month: month,
      },
      true, false
    ).then(res => {
      let data = res.data.record
      let gradeNumTemp = data.grade;
      let gradetemp = '';
      let gradeX = 0;
      if (gradeNumTemp <= 500) {
        gradetemp = '青铜';
      } else if (gradeNumTemp <= 1000) {
        gradetemp = '白银';
      } else if (gradeNumTemp <= 2000) {
        gradetemp = '黄金';
      } else if (gradeNumTemp <= 3000) {
        gradetemp = '钻石';
      } else if (gradeNumTemp <= 4000) {
        gradetemp = '皇冠';
      } else if (gradeNumTemp <= 5000) {
        gradetemp = '学霸';
      } else {
        let tempNum = parseInt((gradeNumTemp - 5000) / 100);
        let tempcon = 0;
        for (let i = 0; i < tempNum; i++) {
          tempcon++
        }
        gradeX = tempcon;
        gradetemp = '学神';// (' + (tempcon ? tempcon + '★' : '') + ')';
      }
      that.setData({
        userGrade: {
          gradeNum: gradeNumTemp,
          grade: gradetemp,
          gradeX: gradeX
        },
      })
      if (data.nickName) {
        that.setData({
          myname: data.nickName,
          myurl: data.avatarUrl
        })
      }
    })
    //     if (l[i].openid == openid) {
    //       that.setData({
    //         userGrade: {
    //           url: l[i].user_grade[0].avatarUrl,
    //           name: l[i].user_grade[0].nickName,
    //           index: tempI,
    //           gradeNum: gradeNumTemp,
    //           grade: gradetemp,
    //           gradeX: gradeX
    //         }
    //       })
    //     }
  },
  goRecords() {
    wx.navigateTo({
      url: '../records/records'
    })
  },

  get_list() {
    let today = new Date();
    let month = today.getMonth() + 1;
    // let dateTemp = 0;
    // if (month <= 3) {
    //   dateTemp = 1
    // } else if (month <= 6) {
    //   dateTemp = 2;
    // } else if (month <= 9) {
    //   dateTemp = 3;
    // } else {
    //   dateTemp = 4;
    // }
    req.get(
      requestUrl.gradeList, {
        year: today.getFullYear(),
        month: month,
        pageNo: this.data.page,
        pageSize: this.data.limit
      },
      true, false
    ).then(res => {
      let that = this;
      console.log('========================战力排行数据源=========================');
      console.log(res);
      let info = res.data;
      if (info) {
        let listTemp = [];
        let l = info.records;
        let gradeNumTemp = 0;
        let tempI = this.data.rank;
        for (let i = 0; i < l.length; i++) {
          gradeNumTemp = l[i].grade;
          tempI++;
          // if (gradeNumTemp == l[i].grade) {
          //   gradeNumTemp = l[i].grade;
          // } else {
          //   tempI++;
          //   gradeNumTemp = l[i].grade;
          // }
          let gradetemp = '';
          let gradeX = 0;
          if (gradeNumTemp <= 500) {
            gradetemp = '青铜';
          } else if (gradeNumTemp <= 1000) {
            gradetemp = '白银';
          } else if (gradeNumTemp <= 2000) {
            gradetemp = '黄金';
          } else if (gradeNumTemp <= 3000) {
            gradetemp = '钻石';
          } else if (gradeNumTemp <= 4000) {
            gradetemp = '皇冠';
          } else if (gradeNumTemp <= 5000) {
            gradetemp = '学霸';
          } else {
            let tempNum = parseInt((gradeNumTemp - 5000) / 100);
            let tempcon = 0;
            for (let i = 0; i < tempNum; i++) {
              tempcon++
            }
            gradeX = tempcon;
            gradetemp = '学神';// (' + (tempcon ? tempcon + '★' : '') + ')';
          }
          if (l[i].nickName != null) {
            listTemp.push({
              url: l[i].avatarUrl,
              name: l[i].nickName,
              index: tempI,
              gradeNum: gradeNumTemp,
              grade: gradetemp,
              gradeX: gradeX
            })
          } else {
            listTemp.push({
              url: '',
              name: '未注册用户',
              index: tempI,
              gradeNum: gradeNumTemp,
              grade: gradetemp,
              gradeX: gradeX
            })
          }
        }
        // console.log("========================战力排行======================");
        // console.log(listTemp);
        this.setData({
          list: this.data.list.concat(listTemp),
          total: info.total,
          rank: tempI
        })
      }
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '战力排行'
    })
    this.get_my();
    this.get_list();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

    if (this.data.list.length == this.data.total) {
      wx.showToast({
        title: '没有更多',
        icon: 'none'
      })
      return;
    }
    this.setData({
      page: this.data.page + 1
    })
    this.get_list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  }
})