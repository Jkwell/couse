Page({

  /**
   * 页面的初始数据
   */
  data: {
    create_date: '',
    myurl: '',
    myname: '微信用户',
    islogin: false,
    grade: []
  },
  logout: function () {
    wx.setStorageSync('Authorization', null);
    wx.setStorageSync('userinfo', null)
    wx.setStorageSync('loginType', null)


    this.setData({
      islogin: false
    })
    wx.navigateBack()
  },
  goVip() {
    wx.navigateTo({
      url: '../member/member'
    })
  },
  
  getDW: function (g) {
    let gradetemp = '';
    if (g <= 500) {
      gradetemp = '青铜';
    } else if (g <= 1000) {
      gradetemp = '白银';
    } else if (g <= 2000) {
      gradetemp = '黄金';
    } else if (g <= 3000) {
      gradetemp = '钻石';
    } else if (g <= 4000) {
      gradetemp = '皇冠';
    } else if (g <= 5000) {
      gradetemp = '学霸';
    } else {
      let tempNum = parseInt((g - 5000) / 100);
      let tempcon = 0;
      for (let i = 0; i < tempNum; i++) {
        tempcon++
      }
      gradetemp = '学神 (' + (tempcon ? tempcon + '★' : '') + ')';
    }
    return gradetemp;
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userinfo = wx.getStorageSync('userInfo');
    if (userinfo) {
      this.setData({
        islogin: true
      })
    }
    wx.setNavigationBarTitle({
      title: '个人中心'
    })
    wx.cloud.callFunction({ name: 'api', data: { fn: 'get_user' } }).then(res => {
      let info = res.result;
      if (info) {
        let date = new Date(info.data[0].created_date)
        this.setData({
          myurl: info.data[0].avatarUrl,
          myname: info.data[0].nickName,
          create_date: date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日'
        })
        wx.cloud.callFunction({ name: 'api', data: { fn: 'get_gradeall' } }).then(res => {
          if (res.result) {
            let tempV = [];
            let data = res.result.data;
            for (let i = 0; i < data.length; i++) {
              tempV.push({
                gradeV: this.getDW(data[i].grade),
                ...data[i]
              })
            }
            this.setData({
              grade: tempV
            })
          }
        })



      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  }
})