// pages/custom/custom.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    _id: null, // 当前单词id
    right: null, // 正确答案
    all_list: [], // 全部单词
    index: 0, // 当前单词
    list: [], // 四选一的列表
    mode: 1, // 模式 一共6中  1，2，3，4，5，6
  },
  next() {
    let index = this.data.index;

    if (index == (this.data.all_list.length * 6 - 1)) {
      return wx.showModal({
        title: '全部练习完毕'
      })
    } else {
      index = index + 1
    }
    this.setData({
      index
    })
    let key = this.data.index % 6
    
    let _id = this.data.all_list[key]
    
    // switch(key){
    //   case 1:
    //     this.get_one(_id)
    //   case 2:
    //     this.get
    // }
   
  },

  // 播放音频
  play_audio(e) {
    let _id = e.currentTarget.dataset._id
    let word = e.currentTarget.dataset.word;
    let type = e.currentTarget.dataset.type;
    if (this.audio) {
      this.audio.stop()
    }
    this.audio = wx.createInnerAudioContext({
      useWebAudioImplement:true
    })
    this.audio.src = "http://dict.youdao.com/dictvoice?audio=" + word //+ '&type=' + type
    this.audio.play()
    wx.showModal({
      title: _id == this.data.right._id ? '正确' : '错误',
      content: '正确答案为' + this.data.right.vc_interpretation
    })
  },
  // 六种模式之二 给语音 认英文
  // 六种模式之三 给中文 认英文
  // 六种模式之四 给中文 认语音

  // 六种模式之五 给英文 认语音
  // 六种模式之六 给英文 认中文
  // 六种模式之一 给英文 认释义
  get_one(_id) {
    wx.showLoading();
    let list = []
    this.setData({
      _id: _id
    })
    const db = wx.cloud.database()
    db.collection('word').doc(this.data._id).get()
      .then(res => {
        this.setData({
          right: res.data
        })
        list.push(res.data)
        wx.cloud.callFunction({
          name: 'api',
          data: {
            fn: 'some'
          }
        }).then(res => {
          wx.hideLoading()
          list = list.concat(res.result.list)
          list.sort((a, b) => Math.random() - 0.5)
          this.setData({
            list
          })
        })
      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const eventChannel = this.getOpenerEventChannel()
    eventChannel.on('send', data => {
      let arr = [];
      for (let i = 0; i < data.data.length; i++) {
        for (let j = 0; j < data.data[i].length; j++) {
          arr.push(data.data[i][j]._id)
        }
      }

      this.setData({
        all_list: arr
      })
      this.get_one(this.data.all_list[this.data.index])

    })



  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})