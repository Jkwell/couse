var app = getApp();
var timerId;
var outId;
import req from "../../api/req";
var requestUrl = {
  renDuList: "/kl/word/renduPracticeList",
  answerRecordInsert: "/kl/answerRecord/insert"
}
Page({
  data: {
    index: 0, // 题目序列
    chooseValue: [], // 选择的答案序列
    totalScore: 100, // 总分
    wrong: 0, // 错误的题目数量
    wrongList: [], // 错误的题目集合-乱序
    wrongListSort: [], // 错误的题目集合-正序
    page: 1, //关卡
    num: 60,
    show_style: false,
    second: 10,
  },

  AudioRead(word, type) {
    if (this.audio) {
      this.audio.stop()
    }
    this.audio = wx.createInnerAudioContext({
      useWebAudioImplement: true
    })
    // this.audio.src =
    //  "http://dict.youdao.com/dictvoice?audio=" 
    // + word + '&type=' + type
    this.audio.play()
    this.audio.onPlay((res) => {})
    this.audio.onError((err) => {
      if (type == 2) {
        this.AudioRead(word, 0);
      }
      // if(type == 1){
      //   wx.showToast({
      //     title:'失败',
      //     icon:"error",
      //     duration:2000,
      //   })
      // }
    })

    // var audio = wx.createInnerAudioContext()
    // if(wx.setInnerAudioOption){
    //   wx.setInnerAudioOption({
    //     autoplay: true
    //   })
    // }else{
    //   audio.autoplay = true;
    // }
    // let src = "http://dict.youdao.com/dictvoice?audio=" + word + "&type="+type;
    // audio.src = src;
    // audio.autoplay = true;
    // audio.play();
  },
  play_audio(e) {
    let word = e.currentTarget.dataset.word;
    let type = e.currentTarget.dataset.type || '2';
    // this.AudioRead(word,2);
    if (this.audio) {
      this.audio.stop()
    }
    this.audio = wx.createInnerAudioContext({
      useWebAudioImplement: true
    })
    // this.audio.src = "http://dict.youdao.com/dictvoice?audio=" + word // + '&type=' + 2
    this.audio.src =  word // + '&type=' + 2

    this.audio.play()
  },
  onLoad: function (options) {

    const eventChannel = this.getOpenerEventChannel()
    eventChannel.on('send', data => {

      let arr = [];
      for (let i = 0; i < data.data.length; i++) {
        for (let j = 0; j < data.data[i].length; j++) {
          arr.push(data.data[i][j].id)
        }
      }
      //
      console.log(arr);

      this.setData({
        all_list: arr
      })
      req.post(
        requestUrl.renDuList, arr,
        true, true
      ).then(res => {
        console.log('res', res);
        if (res.code == 200) {
          let qVal = res.data;
          let AA = 0,
            BB = 0,
            CC = 0,
            DD = 0;
          let tempT = '';
          
          for (let i = 0; i < qVal.length; i++) {
            let t = qVal[i].answer;
            tempT += t + ',';
            if (t == 'A') {
              AA++
            } else if (t == 'B') {
              BB++
            } else if (t == 'C') {
              CC++
            } else if (t == 'D') {
              DD++
            }
          }
          console.log('A:' + AA + ',B:' + BB + ',C:' + CC + ',D:' + DD);
          console.log(tempT);
          this.setData({
            questionList: res.data, // arr 拿到答题数据
            // questionList:res.result.list
          })

          
          let count = this.generateArray(0, this.data.questionList.length - 1); // 生成题序

          this.setData({
            shuffleIndex: this.shuffle(count).slice(0, this.data.questionList.length) // 
          })
          var a = this.data.questionList
          var b = this.data.shuffleIndex
          wx.hideLoading()

        }
        
      }).catch(err => {
        if (err.code === 312) {
          wx.showModal({
            title: '提示',
            content: err.message,
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定')
                wx.navigateTo({
                  url: '../member/member',
                })
                // wx.navigateBack({
                //   delta: 0,
                // })
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        //请求报错，或者状态码返回错误。
        wx.hideLoading()
      })

      // wx.cloud.callFunction({
      //   name: 'api',
      //   data: {
      //     fn: 'get_q',
      //     ids: arr
      //   }
      // }).then(res => {
      //   let qVal = res.result.list;
      //   let AA = 0, BB = 0, CC = 0, DD = 0;
      //   let tempT = '';
      //   for (let i = 0; i < qVal.length; i++) {
      //     let t = qVal[i].true;
      //     tempT += t + ',';
      //     if (t == 'A') {
      //       AA ++
      //     } 
      //     else if (t == 'B') {
      //       BB ++
      //     } 
      //     else if (t == 'C') {
      //       CC++
      //     } 
      //     else if (t == 'D') {
      //       DD++
      //     }
      //   }
      //   console.log('A:'+AA+',B:'+BB+',C:'+CC+',D:'+DD);
      //   console.log(tempT);
      //   this.setData({
      //     questionList: res.result.list, // arr 拿到答题数据
      //     // questionList:res.result.list
      //   })


      //   let count = this.generateArray(0, this.data.questionList.length - 1); // 生成题序

      //   this.setData({
      //     shuffleIndex: this.shuffle(count).slice(0, this.data.questionList.length) // 
      //   })
      // })

    })


    // wx.setNavigationBarTitle('答题') // 动态设置导航条标题



  },
  /*
   * 数组乱序/洗牌
   */
  shuffle: function (arr) {
    let i = arr.length;
    while (i) {
      let j = Math.floor(Math.random() * i--);
      [arr[j], arr[i]] = [arr[i], arr[j]];
    }
    return arr;
  },
  /*
   * 单选事件
   */
  radioChange: function (e) {
    let history = this.data.history || {}
    let is_right = this.chooseError(e.detail.value)
    history[this.data.index] = {
      ck: e.detail.value,
      is_right
    }

    this.setData({
      history
    })


    console.log('checkbox发生change事件，携带value值为：', e.detail.value)
    this.data.chooseValue[this.data.index] = e.detail.value;
    console.log(this.data.chooseValue);
  },
  /*
   * 多选事件
   */
  checkboxChange: function (e) {
    console.log('checkbox发生change事件，携带value值为：', e.detail.value)
    this.data.chooseValue[this.data.index] = e.detail.value.sort();
    console.log(this.data.chooseValue);
  },
  /*
   * 退出答题 按钮
   */
  outTest: function () {
    wx.showModal({
      title: '提示',
      content: '你真的要退出答题吗？',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
          wx.navigateBack({
            delta: 0,
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  /*
   * 下一题/提交 按钮
   */
  preSubmit() {
    // 渲染上一题
    this.setData({
      index: this.data.index - 1
    })


  },
  nextSubmit: function () {
    this.setData({
      show_style: false,
      second: 10,
    })
    clearInterval(timerId)
    clearTimeout(outId)
    // 如果没有选择
    if (this.data.chooseValue[this.data.index] == undefined || this.data.chooseValue[this.data.index].length == 0) {
      wx.showToast({
        title: '请选择至少一个答案!',
        icon: 'none',
        duration: 2000,
        success: function () {
          return;
        }
      })
      return;
    }

    // 判断答案是否正确
    this.chooseError2();

    // 判断是不是最后一题
    if (this.data.index < this.data.shuffleIndex.length - 1) {
      // 渲染下一题
      this.setData({
        index: this.data.index + 1
      })
    } else {
      // wx.showToast({
      //   title: '已经是最后一题',
      //   icon:'none'
      // })

      wx.showModal({
        title: '提示',
        content: '确认提交',
        confirmColor: "#ff0000",
        success: res => {
          if (res.confirm) {
            // 提交
            this.setData({
              wrong: this.wrong(),
              totalScore: this.totalScore(),
            })

            // let wrongList = JSON.stringify(this.data.wrongList);
            // let wrongListSort = JSON.stringify(this.data.wrongListSort);
            // let chooseValue = JSON.stringify(this.data.chooseValue);
            // let questionList = JSON.stringify(this.data.questionList)
            // console.log('questionList', questionList);
            // console.log('totalScore', this.data.totalScore);
            wx.setStorageSync('wrongList', this.data.wrongList);
            wx.setStorageSync('wrongListSort', this.data.wrongListSort);
            wx.setStorageSync('chooseValue', this.data.chooseValue);
            wx.setStorageSync('questionList', this.data.questionList);
            let bookInfo = wx.getStorageSync('dict')
            let integral = this.data.questionList.length - this.data.wrongList.length
            req.post(
              requestUrl.answerRecordInsert, {
                correctNum: integral,
                errorNum: this.data.wrongList && this.data.wrongList.length,
                integral: integral,
                questionBank:bookInfo.title,
                questionNum: this.data.questionList && this.data.questionList.length,
                score: this.data.totalScore
              },
              true, true
            ).then((res) => {
              console.log('resINsert', res);
              if (res.code === 200) {
                wx.redirectTo({
                  url: './results?totalScore=' + this.data.totalScore
                })
              }
            })
            

            // 设置缓存
            var logs = wx.getStorageSync('logs') || []
            let logsList = {
              "date": Date.now(),
              "testId": this.data.testId,
              "score": this.data.totalScore
            }
            logs.unshift(logsList);
            wx.setStorageSync('logs', logs);
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })

    }
  },
  /*
   * 错题处理
   */
  chooseError2: function () {
    var trueValue = this.data.questionList[this.data.shuffleIndex[this.data.index]]['answer'];
    var chooseVal = this.data.chooseValue[this.data.index];
    console.log('选择了' + chooseVal + '答案是' + trueValue);



    if (chooseVal.toString() != trueValue.toString()) {
      console.log('错了');
      this.data.wrong++;
      // 已经有了就别push了
      let have = false;
      this.data.wrongListSort.forEach(v => {
        if (this.data.index == v) {
          have = true
        }
      })
      if (!have) {
        this.data.wrongListSort.push(this.data.index);
        this.data.wrongList.push(this.data.shuffleIndex[this.data.index]);
      }



      // this.setData({
      //   totalScore: this.data.totalScore - this.data.questionList[this.data.shuffleIndex[this.data.index]]['scores'] // 扣分操作
      // })
    }
  },
  wrong() {
    let t = 0;
    for (let key in this.data.history) {
      if (!this.data.history[key].is_right) {
        t += 1
      }
    }
    return t;
  },
  totalScore() {
    let t = 0;
    for (let key in this.data.history) {
      if (this.data.history[key].is_right) {
        t += 10
      }
    }
    return t;
  },
  chooseError: function (chooseVal) {
    console.log('this.data.questionList', this.data.questionList);
    var trueValue = this.data.questionList[this.data.shuffleIndex[this.data.index]]['answer'];
    // var chooseVal = this.data.chooseValue[this.data.index];
    console.log('选择了' + chooseVal + '答案是' + trueValue);
    this.setData({
      show_style: true
    })
    timerId = setInterval(() => {
      this.setData({
        second: this.data.second - 1
      })
    }, 1000)
    // 10秒后自动跳转下一题
    outId = setTimeout(this.nextSubmit, 10000)

    console.log('ss', chooseVal.toString() == trueValue.toString());
    return chooseVal.toString() == trueValue.toString()
    if (chooseVal.toString() != trueValue.toString()) {
      console.log('错了');
      // this.data.wrong++;
      // this.data.wrongListSort.push(this.data.index);
      // this.data.wrongList.push(this.data.shuffleIndex[this.data.index]);
      // this.setData({
      //   totalScore: this.data.totalScore - this.data.questionList[this.data.shuffleIndex[this.data.index]]['scores'] // 扣分操作
      // })
    }
  },
  /**
   * 生成一个从 start 到 end 的连续数组
   * @param start
   * @param end
   */
  generateArray: function (start, end) {
    return Array.from(new Array(end + 1).keys()).slice(start)
  },
  onunload() {
    clearInterval(timerId)
    clearTimeout(outId)
  }
})