import req from "../../api/req";
var app = getApp();  //声明app实例
var requestUrl = {
  login: "/auth/login"
}
Page({

  /**
   * 页面的初始数据
   */
  data: {

    // let category_id = options.category_id || 43
    // let title = options.title || '专四核心词汇'
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    loginType: false,
    myurl: '',
    myname: '微信用户',
    grade: '青铜',
    gradeNum: 0,
    list: [{
        title: '单词认读练习',
        img: 'https://image.weilanwl.com/color2.0/plugin/sylb2244.jpg',
        url: '../read/read?category_id=1ace8ef160aa1b220143e32b6e0854d8&title=单词认读'
      },
      {
        title: '单词拼写练习',
        img: 'https://image.weilanwl.com/color2.0/plugin/wdh2236.jpg',
        url: '/animation/animation'
      },
      {
        title: '自定义练习',
        img: 'https://image.weilanwl.com/color2.0/plugin/qpct2148.jpg',
        url: '../category/category'
      }
    ]
  },
  getUserImg: function (e) {},
  goGrade() {
    wx.navigateTo({
      url: '../user/info'
    })
  },
  toChild(e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    })
  },
  logout: function () {
    this.setData({
      loginType: false
    })
  },
  getUserFunc: function (r) {
    let that = this;
    wx.getUserProfile({
      desc: '正在获取',
      success: function (res) {
        let resV = res.userInfo;
        wx.login({
          success(res) {
            if (res.code) {
              req.post(
                requestUrl.login, {
                  code: res.code,
                  avatarUrl: resV.avatarUrl,
                  nickName: resV.nickName
                },
                true, true
              ).then(res => {
                if (res.code == 200) {
                  wx.setStorageSync('Authorization', res.data.Authorization);
                  app.globalData.token = res.data.Authorization
                  wx.setStorageSync('userinfo', res.data.userinfo)
                  wx.setStorageSync('loginType', true)
                  getApp().globalData.token = res.data.Authorization
                  that.setData({
                    myname: wx.getStorageSync('userinfo').nickName,
                    myurl: wx.getStorageSync('userinfo').avatarUrl,
                    loginType: wx.getStorageSync("loginType")
                  })
                }
              }).catch(err => {
                //请求报错，或者状态码返回错误。
              })
            }
          },
          fail: function () {
            console.log("发送code失败：", res.data);
          }
        })
      },
      fail: function () {
        console.log("用户拒绝获取昵称头像")
      }
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // let that = this;
    // that.setData({
    //   myname: wx.getStorageSync('userinfo').nickName,
    //   myurl: wx.getStorageSync('userinfo').avatarUrl,
    //   loginType: wx.getStorageSync("loginType")
    // })



    // wx.cloud.callFunction({
    //   name: 'api',
    //   data: {
    //     fn: 'get_user'
    //   }
    // }).then(res => {
    //   if (res.result.data.length == 0) {
    //     wx.setStorageSync('userInfo', null)
    //     that.setData({
    //       loginType: false
    //     })
    //   } else {

    //     if (user) {
    //       that.setData({
    //         myname: res.result.data[0].nickName,
    //         myurl: res.result.data[0].avatarUrl,
    //         loginType: true
    //       })
    //     } else {
    //       that.setData({
    //         loginType: false
    //       })
    //     }
    //   }
    // })
    let x = wx.getStorageSync('dict')
    if (x) {
      if (x.title.length >= 12) {
        x.title = x.title.slice(0, 12) + '..'
      }
      this.setData({
        title: x.title
      })
    }
    // wx.cloud.callFunction({
    //   name: 'api',
    //   data: {
    //     fn: 'get_openid'
    //   }
    // }).then(res => {
    //   wx.setStorageSync('openid', res.result)
    //   this.setData({
    //     openid: res.result
    //   })

    // })



  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that =this;
    let user = wx.getStorageSync('userInfo')
    if (user == null || user == '') {
      that.setData({
        loginType: false
      })
    }
    let today = new Date();
    let month = today.getMonth() + 1;
    let dateTemp = 0;
    if (month <= 3) {
      dateTemp = 1
    } else if (month <= 6) {
      dateTemp = 2;
    } else if (month <= 9) {
      dateTemp = 3;
    } else {
      dateTemp = 4;
    }
    // wx.cloud.callFunction({
    //   name: 'api',
    //   data: {
    //     fn: 'update_grade',
    //     data:{
    //       id:'17e3426e61e013cc05ea91632612a6a2',
    //       grade: 3
    //     }
    //   }
    // }).then(res => {
    //   console.log(res);
    // })


    wx.cloud.callFunction({
      name: 'api',
      data: {
        fn: 'get_grade',
        data: {
          month: dateTemp,
          year: today.getFullYear()
        }
      }
    }).then(res => {
      if (res.result.data == 0) {
        // wx.cloud.callFunction({ name: 'api', data: { fn: 'get_user' } }).then(res => {
        //   if (res.result.data.length > 0) {

        //   }
        // })
        wx.cloud.callFunction({
          name: 'api',
          data: {
            fn: 'add_grade',
            data: {
              openid: wx.getStorageSync('openid'),
              year: today.getFullYear(),
              month: dateTemp,
              create_date: today,
              grade: 0
            }
          }
        })
      } else {
        let gradeNumTemp = res.result.data[0].grade;
        let gradetemp = '';
        let gradeX = 0;
        if (gradeNumTemp <= 500) {
          gradetemp = '青铜';
        } else if (gradeNumTemp <= 1000) {
          gradetemp = '白银';
        } else if (gradeNumTemp <= 2000) {
          gradetemp = '黄金';
        } else if (gradeNumTemp <= 3000) {
          gradetemp = '钻石';
        } else if (gradeNumTemp <= 4000) {
          gradetemp = '皇冠';
        } else if (gradeNumTemp <= 5000) {
          gradetemp = '学霸';
        } else {
          let tempNum = parseInt((gradeNumTemp - 5000) / 100);
          let tempcon = 0;
          for (let i = 0; i < tempNum; i++) {
            tempcon++
          }
          gradeX = tempcon;
          // gradetemp = '学神';// (' + (tempcon ? tempcon + '★' : '') + ')';
          gradetemp = '学神 ' + (tempcon ? tempcon + '★' : '') + '';
        }
        this.setData({
          grade: gradetemp,
          gradeNum: gradeNumTemp,
          gradeX: gradeX
        })
        wx.setStorageSync('gradeNum', gradeNumTemp);
        wx.setStorageSync('gradeId', res.result.data[0]._id);
      }
    })

    this.setData({
      myname: wx.getStorageSync('userinfo').nickName,
      myurl: wx.getStorageSync('userinfo').avatarUrl,
      loginType: wx.getStorageSync("loginType")
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '每天一小时，半个月记住一学期单词'
    }
  }
})