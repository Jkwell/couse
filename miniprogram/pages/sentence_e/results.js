// pages/results/results.js
var app = getApp();
Page({
  data: {
    totalScore: 0, // 分数
    wrongList: [], // 错误的题数-乱序
    wrongListSort: [], // 错误的题数-正序
    chooseValue: [], // 选择的答案
    remark: ["恭喜过关", "恭喜过关", "得分没有超过80，继续努力哦！"], // 评语
    modalShow: false
  },
  onLoad: function (options) {
    this.setData({
      location_id: options.location_id
    })
    // wx.setNavigationBarTitle({ title: options.testId }) // 动态设置导航条标题

    // let wrongList = JSON.parse(options.wrongList);
    // let wrongListSort = JSON.parse(options.wrongListSort);
    // let chooseValue = JSON.parse(options.chooseValue);
    // let questionList = JSON.parse(options.questionList)

    let wrongList = wx.getStorageSync('wrongList')
    let wrongListSort = wx.getStorageSync('wrongListSort')
    let chooseValue = wx.getStorageSync('chooseValue')
    let questionList = wx.getStorageSync('questionList')

    wx.removeStorageSync('wrongList');
    wx.removeStorageSync('wrongListSort');
    wx.removeStorageSync('chooseValue');
    wx.removeStorageSync('questionList');
    let arr2 = []
    let dict = {}
    wrongList.forEach(v => {
      if (!dict[questionList[v].question.sentence_en]) {
        dict[questionList[v].question.sentence_en] = true
        arr2.push({
          danci: questionList[v].question.sentence_en,
          shiyi: questionList[v].question.sentence_zh
        })
      }
    })
    this.setData({
      ll: arr2
    })


    this.setData({
      totalScore: options.totalScore != "" ? options.totalScore : "无",
      wrongList: wrongList,
      wrongListSort: wrongListSort,
      chooseValue: chooseValue,
      questionList: questionList, // 拿到答题数据
    })

    let tempNum = this.data.questionList.length - this.data.wrongList.length;
    let gradeId = wx.getStorageSync('gradeId')
    let gradeNum = wx.getStorageSync('gradeNum')
    if (gradeId) {
      gradeNum += tempNum
      wx.cloud.callFunction({
        name: 'api', data: {
          fn: 'update_grade',
          data: {
            id: gradeId,
            grade: gradeNum
          }
        }
      }).then(res => {
        wx.setStorageSync('gradeNum', gradeNum)
      })
    }


  },
  // 查看错题
  toView: function () {
    // 显示弹窗
    this.setData({
      modalShow: true
    })
  },
  // 返回首页
  toIndex: function () {
    wx.reLaunch({
      url: '../index/index'
    })
  }
})