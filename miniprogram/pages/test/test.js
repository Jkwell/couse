// pages/test/test.js
import req from "../../api/req";
var app = getApp(); //声明app实例
var requestUrl = {
  productList: "/kl/product",
  callPay:"/api/wx-pay/xcv",
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: []
  },
  getProductList() {
    let that = this;
    req.get(
      requestUrl.productList, null,
      true, false
    ).then(res => {
      if (res.code == 200) {
        that.setData({
          list: res.data.records
        })
       
      }
    }).catch(err => {
      //请求报错，或者状态码返回错误。
    })
  },
  callpay(e) {
    let productId = e.currentTarget.dataset.product_id
    req.post(
      requestUrl.callPay+'/'+productId, null,
      true, false
    ).then(res => {
      if (res.code == 200) {
        wx.requestPayment({
          timeStamp: res.data.timeStamp,
          nonceStr: res.data.nonceStr,
          package: res.data.package,
          signType: res.data.signType,
          paySign: res.data.paySign,
          success (res) {
            console.log(res)
           },
          fail (res) 
          { 
            console.log(res)
          }
        })
       
      }
    }).catch(err => {
      //请求报错，或者状态码返回错误。
    })


  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let token = wx.getStorageSync('Authorization')
    app.globalData.token = token
    this.getProductList();
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})