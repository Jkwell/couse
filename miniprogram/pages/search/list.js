// pages/product_list/product_list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inputShowed: false,
    inputVal: "",
    where: {
      
    },
    list: [],
    page: 1,
    limit: 20,
    total: 0,
    collection: 'category_sort',
  },
  select(e){
    wx.setStorageSync('dict', {
      _id: e.currentTarget.dataset._id,
      title: e.currentTarget.dataset.title
    })
    wx.showToast({
      title: '选择成功',
      icon: 'success'
    })
    setTimeout(function () {
      wx.reLaunch({
        url: '../index/index',
      })
    }, 500)

  },
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  hideInput: function () {
    this.setData({
      inputVal: "",
      inputShowed: false
    });
    this.setData({
      page: 1,
      list: [],
      where: {
        // is_in_stock: this.data.where.is_in_stock
      }
    })
    this.get_list()
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
    this.setData({
      page: 1,
      list: [],
      where: {
        // is_in_stock: this.data.where.is_in_stock
      }
    })
    this.get_list()
  },
  // 四级
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
    const db = wx.cloud.database()
    this.setData({
      page: 1,
      list: [],
      'where.name': db.RegExp({
        regexp: this.data.inputVal,
        options: 'i',
      })
    })
    this.get_list()
  },

  down(e) {
    let _id = e.currentTarget.dataset.id;
    wx.showLoading();
    wx.cloud.callFunction({
      name: 'eval',
      data: {
        fn: 'update_product',
        _id: _id,
        data: {
          is_in_stock: false
        }
      }
    }).then(res => {
      wx.hideLoading();
      wx.showToast({
        title: '下架成功',
      })
      let list = this.data.list;
      list = list.filter(v => v._id != _id)
      this.setData({
        list: list
      })
    })
  },

  up(e) {
    let _id = e.currentTarget.dataset.id;
    wx.showLoading();
    wx.cloud.callFunction({
      name: 'eval',
      data: {
        fn: 'update_product',
        _id: _id,
        data: {
          is_in_stock: true
        }
      }
    }).then(res => {
      wx.hideLoading();
      wx.showToast({
        title: '上架成功',
      })
      let list = this.data.list;
      list = list.filter(v => v._id != _id)
      this.setData({
        list: list
      })
    })
  },
  set(e) {
    let item = e.currentTarget.dataset.item;
    
    this.setData({
      'where.is_in_stock':  Boolean(item),
      page: 1,
      nomore: false,
      list: []
    })
    this.get_list();
  },

  get_list() {
    wx.showLoading();
    const db = wx.cloud.database();
    // 读取总数
    db.collection(this.data.collection)
      .where(this.data.where)
      .count()
      .then(res => {
        this.setData({
          total: res.total
        })
      })
    // 读取列表
    db.collection(this.data.collection)
      .where(this.data.where)
      .orderBy('category_type', 'asc')
      // .orderBy('created_at', 'desc')
      .skip((this.data.page - 1) * this.data.limit)
      .limit(this.data.limit)
      // .field({
      //   name: true,
      //   main_image: true,
      //   price: true,
      //   is_in_stock: true,
      // })
      .get()
      .then(res => {
        wx.hideLoading()
        if (res.data.length) {
          this.setData({
            list: this.data.list.concat(res.data)
          })
        }
      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.get_list();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.list.length == this.data.total) {
      this.setData({
        nomore: true
      })
      return;
    }
    this.setData({
      page: this.data.page + 1
    })
    this.get_list();
  }

})