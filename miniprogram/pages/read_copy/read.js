const db = wx.cloud.database();
Page({
  /**
   * 页面的初始数据
   */
  data: {

    status: ['未练习', '已练习'],
    cu: '未练习',
    gridCol: 5,
    show: false,
    t: 2,
    where: {},
    page: 1,
    limit: 100,
    total: 0,
    list: [],
    display: [],
    display_already: [],
    choose: {},
  },
  select(e) {
    let choose = this.data.choose
    if (!choose[e.currentTarget.dataset.index]) {
      choose[e.currentTarget.dataset.index] = true
    } else {
      choose[e.currentTarget.dataset.index] = false
    }

    this.setData({
      choose,
    })

  },

  go() {
    // 把单词全部记录到本地
    let arr = wx.getStorageSync('display_alread') || []

    for (let k in this.data.choose) {
      if (k) {
        arr.push(this.data.display[k])
      }
    }
    this.setData({
      display_already: arr
    })
    wx.setStorageSync('display_alread', this.data.display_already)

    let arr2 = []
    for (let k in this.data.choose) {
      if (k) {
        arr2.push(this.data.display[k])
      }
    }
    if (arr2.length) {
      wx.navigateTo({
        url: '../custom/custom',
        success: function (res) {
          res.eventChannel.emit('send', {
            data: arr2
          })
        }
      })
    } else {
      wx.showModal({
        title: '请选择单词'
      })
    }

  },

  play_audio(e) {
    let word = e.currentTarget.dataset.word;
    let type = e.currentTarget.dataset.type;
    if (this.audio) {
      this.audio.stop()
    }
    this.audio = wx.createInnerAudioContext({
      useWebAudioImplement:true
    })
    this.audio.src = "http://dict.youdao.com/dictvoice?audio=" + word// + '&type=' + type
    this.audio.play()
    wx.showModal({
      title: '释义',
      content: e.currentTarget.dataset.vc_interpretation
    })
  },

  pop(e) {
    wx.showModal({
      title: '释义',
      content: e.currentTarget.dataset.vc_interpretation
    })
  },

  set_cu(e) {
    this.setData({
      cu: e.currentTarget.dataset.cu
    })
    if (this.data.cu == '未练习') {
      this.setData({
        list: [],
        page: 1,
        total: 0,
        choose: {}
      })
      this.get_list();
    }

  },




  get_list() {
    if (this.data.t == 1) {
      var fn = 'get_user_word'
    } else if (this.data.t == 2) {
      var fn = 'get_not_user_word'
    }

    wx.showLoading({
      title: '正在加载'
    })
    // 把该词典中已练习的单词发过去 过滤掉

    console.log(fn)
    wx.cloud.callFunction({
      name: 'api',
      data: {
        fn,
        where: this.data.where,
        page: this.data.page,
        limit: this.data.limit,
        display_already: this.data.display_already
      }
    }).then(res => {
      wx.hideLoading({
        success: (res) => {},
      })
      this.setData({
        list: this.data.list.concat(res.result.list),
        total: res.result.total
      })
      let display = []
      let group = this.data.list.length / 10
      for (let i = 0; i < group; i++) {
        display.push(this.data.list.slice(i * 10, i * 10 + 10))
      }

      this.setData({
        display,
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {


    wx.cloud.callFunction({
      name: 'api',
      data: {
        fn: 'get_openid'
      }
    }).then(res => {
      this.setData({
        openid: res.result
      })
    })

    let category_id = options.category_id || 43
    let title = options.title || '专四核心词汇'
    wx.setNavigationBarTitle({
      title,
    })

    // 记录打开了哪本词典
    let history_list = wx.getStorageSync('history') || {}
    history_list[category_id] = title
    wx.setStorageSync('history', history_list)

    // 找出该词典的已练习
    let display_already = wx.getStorageSync('display_alread') || []
    display_already = display_already.filter(v => v[0].bookId == category_id)


    this.setData({
      'title': title,
      'where.bookId': category_id,
      'display_already': display_already
    })
    this.get_list();



  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    // 找出该词典的已练习
    let display_already = wx.getStorageSync('display_alread') || []
    display_already = display_already.filter(v => v[0].bookId == this.data.where.bookId)
    this.setData({
      display_already
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.cu == '已练习') {
      return;
    }
    if (this.data.list.length == this.data.total) {
      wx.showToast({
        title: '没有更多',
        icon: 'none'
      })
      return;
    }
    this.setData({
      page: this.data.page + 1
    })
    this.get_list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: this.data.title
    }
  },
  onShareTimeline() {
    return {
      title: this.data.title
    }
  }
})